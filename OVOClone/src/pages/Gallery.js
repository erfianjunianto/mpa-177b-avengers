import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Left, Body, Right, Title, Card, CardItem, Text, Thumbnail } from 'native-base';
import { Actions } from 'react-native-router-flux';


export default class Gallery extends Component {
    constructor(props){
        super(props);
        this.state={
            terimaData: this.props.kirimData,
        }
    }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=>Actions.home()}>
              <Icon type="MaterialIcons" name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Gallery</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="MaterialIcons" name='menu' />
            </Button>
          </Right>
        </Header>
        <Content padder>

          <Text>Gallery - {this.props.kirimData} - {this.props.kirimData2}</Text>

        </Content>
        <Footer>
          <FooterTab>
            <Button onPress={() => Actions.home()}>
              <Icon type="AntDesign" name="appstore-o" />
            </Button>
            <Button>
              <Icon type="AntDesign" name="camera" />
            </Button>
            <Button>
              <Icon type="FontAwesome" name="thumbs-o-up" />
            </Button>
            <Button>
              <Icon type="MaterialIcons" name="person" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}