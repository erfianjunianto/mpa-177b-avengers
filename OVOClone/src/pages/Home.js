import React, { Component } from 'react';
import {Image, StyleSheet} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Left, Body, Right, Title, Card, CardItem, Text, Thumbnail } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.headerStyle}>
          <Left>
            <Button transparent>
              <Icon type="MaterialIcons" name='home' />
            </Button>
          </Left>
          <Body>
            <Title style={{fontFamily:"vinchand", fontSize:30}}>Home</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="MaterialIcons" name='menu' />
            </Button>
          </Right>
        </Header>
        <Content padder>

          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItemTopStyle}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>MPA-177B</Text>
                  <Text note>Avengers</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{ uri: 'https://images.unsplash.com/photo-1600937543638-e35f9efb1564?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80' }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem style={styles.cardItemBottomStyle}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-o-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>

          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItemTopStyle}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>MPA-177B</Text>
                  <Text note>Avengers</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{ uri: 'https://images.unsplash.com/photo-1600019402325-c7a7ee7cd0dd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem style={styles.cardItemBottomStyle}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-o-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>

          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItemTopStyle}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>MPA-177B</Text>
                  <Text note>Avengers</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{ uri: 'https://images.unsplash.com/photo-1601055283742-8b27e81b5553?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem style={styles.cardItemBottomStyle}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-o-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>

        </Content>
        <Footer>
          <FooterTab style={{backgroundColor:"red"}}>
            <Button>
              <Icon type="AntDesign" name="appstore-o" />
            </Button>
            <Button onPress={() => Actions.gallery({kirimData:"data ini akan dikirim melalui props tanpa state", kirimData2:"ini data kedua"})}>
              <Icon type="AntDesign" name="camera" />
            </Button>
            <Button>
              <Icon type="FontAwesome" name="thumbs-o-up" />
            </Button>
            <Button>
              <Icon type="MaterialIcons" name="person" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerStyle:{
    backgroundColor:"#d91e18",
  },

  cardStyle:{
    borderTopLeftRadius:25,
    borderBottomRightRadius:25,
  },

  cardItemTopStyle:{
    borderTopLeftRadius:25,
    backgroundColor:"#22313f"
  },

  cardItemBottomStyle:{
    borderBottomRightRadius:25
  }
});